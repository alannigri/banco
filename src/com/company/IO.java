package com.company;

import java.util.Scanner;

public class IO {
    Scanner scanner = new Scanner(System.in);

    public String setNome() {
        System.out.println("Nome:");
        String nome = scanner.next();
        return nome;
    }

    public int setIdade() {
        System.out.println("Idade:");
        int idade = scanner.nextInt();
        return idade;
    }

    public double setSaldo() {
        System.out.println("Saldo inicial:");
        double saldo = scanner.nextDouble();
        return saldo;
    }

    public int menu() {
        System.out.println("Digite 1 para sacar, 2 para depositar, 3 para consultar saldo, 4 para finalizar");
        int menu = scanner.nextInt();
        return menu;
    }

    public void exibeSaldo(double valor) {
        System.out.println("Saldo de R$" + valor);
    }

    public double valor() {
        System.out.println("Informe o valor:");
        double valor = scanner.nextDouble();
        return valor;
    }

    public static String menorIdade() {
        return "Menor de idade!";
    }
}
