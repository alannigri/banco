package com.company;

public class Cliente {
    private String nome;
    private int idade;

    public Cliente(String nome, int idade) {
        if (!validaIdade(idade)) {
            throw new ArithmeticException(IO.menorIdade());
        } else {
            this.nome = nome;
            this.idade = idade;
        }
    }

    public boolean validaIdade(int idade) {
        if (idade < 18) {
            return false;
        } else {
            return true;
        }
    }
}
