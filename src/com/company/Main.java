package com.company;

public class Main {

    public static void main(String[] args) {
        IO io = new IO();
        Cliente cliente = new Cliente(io.setNome(), io.setIdade());
        Banco banco = new Banco(io.setSaldo());
        int menu;
        do {
            menu = io.menu();
            if (menu == 1) {
                banco.sacar(io.valor());
                io.exibeSaldo(banco.getSaldo());
            } else if (menu == 2) {
                banco.depositar(io.valor());
                io.exibeSaldo(banco.getSaldo());
            } else if (menu == 3) {
                io.exibeSaldo(banco.getSaldo());
            }
        } while (menu != 4);

    }
}

